# phase-0-gps-1
This is my README

##Commands
cd unit-1
ls
git clone LINK
touch awesome_page.md
git add .
git commit -m "add awesome page"
git push origin master
git checkout --help (q to quit)
subl .

NEW LINE

##something bold

**Hi everyone! we are doing the GPS.**

##something italic
*This is so much fun*

##something code block
```
Richard and Michael are very patient

with me. Thanks guys!
```

#something to link
[Dev Bootcamp > Hack Reactor](http://www.devbootcamp.com)

#something to image
![Alt text](inception.png) "BWWOWOWOWOWO"

#something small-conflict
Michael left! OH NOOOO!!!!

