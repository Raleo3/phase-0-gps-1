#What git concepts were you struggling with prior to the GPS session?

The concepts I was still struggling with were pushd and popd, which weren't even used in the challenge so it was ok. I was more troubled with figuring out how to refer to a previous directory (go back to the last one) because I'd use pushd and popd to do that and I was told of another way. I also struggled with fetch and merge. It still feels like fetch and merge are commands that are alternatives to other commands that I already use (pull and push origin).

#What concepts were clarified during the GPS?

The concepts that I understood better from the GPS were how the ''' git merge ''' command works, the importance of deleting branches for cleaning your workflow, and merge conflicts. Also, committing messages can't have punctuations or there will be errors.

#What questions did you ask your pair and the guide?

The questions I would ask my pair would be to help review over the concepts we had learned prior to the challenge ("So why do you use feature branches?". I would ask the guide about specifics that regarded shortcuts and proper syntax ("How do I open sublime text just from the terminal?")

#What still confuses you about git?

I'm still confused about using fetch and merge when I just use push and pull to do all my work.

#How was your first experience of pairing in a GPS?

I was many things: nervous, anxious, and beaten. These were all emotions I felt from attacking myself. My pair partner and guide both said I did fine and understood the concepts, but I didn't feel confident enough in my presentation. I want to have an ethos that people see as someone they can go to for seeking help. Most of the terms and concepts were not alien to me and I'd say I understood about 95% but I reflect my communication by how nervous I am to talk to someone, so it felt like I understood about 80% of it. I look forward to more so I can be better!