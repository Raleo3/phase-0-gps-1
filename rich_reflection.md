#GPS 1.1 Rich's Reflection
- What git concepts were you struggling with prior to the GPS session?
- What concepts were clarified during the GPS?
- What questions did you ask your pair and the guide?
- What still confuses you about git?
- How was your first experience of pairing in a GPS?

***

I was unclear about the purpose of git fetch.

Michael explained that git fetch was an abbreviated version of git pull, with the added necessity of having to merge locally.

We talked about merge conflicts, and explored the pull request process quite a bit which was very helpful.

Nothing that comes to mind.

Very well. I liked both Michael, our guide, and Farman. Would work with both again in the future.